﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class GoodsTypeDAL : BaseDAL<GoodsType>, IGoodsTypeDAL
    {
        public GoodsTypeDAL(MyDbContext db) : base(db)
        {
        }
    }
}
