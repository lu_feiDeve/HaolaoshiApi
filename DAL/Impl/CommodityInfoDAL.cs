﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class CommodityInfoDAL : BaseDAL<CommodityInfo>, ICommodityInfoDAL
    {
        public CommodityInfoDAL(MyDbContext db) : base(db)
        {
        }
    }
}
