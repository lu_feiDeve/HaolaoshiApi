﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class SchoolTableDAL : BaseDAL<SchoolTable>, ISchoolTableDAL
    {
        public SchoolTableDAL(MyDbContext db) : base(db)
        {
        }
    }
}
