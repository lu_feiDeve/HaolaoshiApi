﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class AccountInfoDAL : BaseDAL<AccountInfo>, IAccountInfoDAL
    {
        public AccountInfoDAL(MyDbContext db) : base(db)
        {
        }
    }
}
