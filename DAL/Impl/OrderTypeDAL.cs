﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class OrderTypeDAL : BaseDAL<OrderType>, IOrderTypeDAL
    {
        public OrderTypeDAL(MyDbContext db) : base(db)
        {
        }
    }
}
