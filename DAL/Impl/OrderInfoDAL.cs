﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class OrderInfoDAL : BaseDAL<OrderInfo>, IOrderInfoDAL
    {
        public OrderInfoDAL(MyDbContext db) : base(db)
        {
        }
    }
}
