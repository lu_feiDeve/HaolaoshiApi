﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class BusinessDAL : BaseDAL<Business>, IBusinessDAL
    {
        public BusinessDAL(MyDbContext db) : base(db)
        {
        }
    }
}
