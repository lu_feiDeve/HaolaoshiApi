﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class GoodsInfoDAL : BaseDAL<GoodsInfo>, IGoodsInfoDAL
    {
        public GoodsInfoDAL(MyDbContext db) : base(db)
        {
        }
    }
}
