﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Web.Extension;
using Web.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Web.Util;
using System.Security.Claims;
using Web.Security;
using System.Drawing.Imaging;

namespace Web.Controllers
{
    /// <summary>
    /// 御键传说。常用的接口
    /// </summary>
    [Route("api/")]
    [ApiController]
    public class AccountInfoLoginController : MyBaseController
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly JwtConfig jwtConfig;
        public IAccountInfoBll accountInfoBll { get; set; }
        public IUserBll userBll { get; set; }//通过属性依赖注入
        public IStudentBll studentBll { get; set; }
        public ITeacherBll teacherBll { get; set; }
        public IImageBll imageBll { get; set; }
        public IClaimsAccessor MyUser { get; set; }
        IConfiguration configuration { get; set; }
        public AccountInfoLoginController(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, IOptions<JwtConfig> jwtConfig)
        {
            this.webHostEnvironment = webHostEnvironment;
            this.jwtConfig = jwtConfig.Value;
            this.configuration = configuration;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="o">接收username、password、type三个参数</param>
        /// <returns></returns>
        // POST: api/accountlogin
        [HttpPost("accountlogin")]
        public Result Login([FromBody] AccountInfo o)
        // public Result Login(string username,string password,string type="student")
        {
            //设账户信息为Null
            AccountInfo obj = null;
            //当存在该账户名以及密码时  返回该用户名以及密码 并存放到obj变量中
            obj = accountInfoBll.Login(o.AccountName, o.AccountPswd);
            //若obj不等于null则登录成功
            if (obj != null)
            {
                return Result.Success("登录成功");
                       
            }
            //反之登录失败
            return Result.Error("登录失败");
        }
    }
}