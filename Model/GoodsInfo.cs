﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Serializable]
    [Table("GoodsInfo")]
    public class GoodsInfo:ID
    {
        //商品类型名称
        public string Name{ get; set; }
        //商品图片
        public string Pic { get; set; }
        //商品类型
        public int? GoodsTypeId { get; set; }
        [ForeignKey("GoodsTypeId")]
        public virtual GoodsType GoodsType { get; set; }
        //商店
        public int? BusinessId { get; set; }
        [ForeignKey("BusinessId")]
        public virtual Business Business { get; set; }
        //价格
        public int Price { get; set; }
    }
}
