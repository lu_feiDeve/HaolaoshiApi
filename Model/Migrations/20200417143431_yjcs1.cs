﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class yjcs1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    WxID = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    AccountTel = table.Column<string>(nullable: true),
                    AccountAddress = table.Column<string>(nullable: true),
                    AccountPswd = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    Expanded = table.Column<bool>(nullable: false),
                    Common = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Area_Area_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: false),
                    Size = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    TypeName = table.Column<string>(nullable: true),
                    OrderTypeDesc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SchoolTable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    SchoolName = table.Column<string>(nullable: true),
                    SchoolNumber = table.Column<int>(nullable: false),
                    Register_Time = table.Column<DateTime>(nullable: false),
                    Partition = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Business",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    BuName = table.Column<string>(nullable: true),
                    BuImage = table.Column<string>(nullable: true),
                    AccountInfoId = table.Column<int>(nullable: true),
                    BuAddress = table.Column<string>(nullable: true),
                    PermitImage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Business_AccountInfo_AccountInfoId",
                        column: x => x.AccountInfoId,
                        principalTable: "AccountInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Username = table.Column<string>(maxLength: 20, nullable: false),
                    Pswd = table.Column<string>(maxLength: 40, nullable: false),
                    Realname = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: false),
                    Tel = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false),
                    QQ = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    Nation = table.Column<string>(nullable: true),
                    IDCard = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    AreaId = table.Column<int>(nullable: true),
                    Available_balance = table.Column<double>(nullable: true),
                    Integral = table.Column<int>(nullable: false),
                    Gold = table.Column<int>(nullable: false),
                    Friend_num = table.Column<int>(nullable: false),
                    Attention_num = table.Column<int>(nullable: false),
                    Attentioned_num = table.Column<int>(nullable: false),
                    Credit = table.Column<int>(nullable: false),
                    Last_login_date = table.Column<DateTime>(nullable: false),
                    Login_date = table.Column<DateTime>(nullable: false),
                    Last_login_ip = table.Column<string>(nullable: true),
                    Login_ip = table.Column<string>(nullable: true),
                    Login_count = table.Column<int>(nullable: false),
                    IMEI = table.Column<string>(nullable: true),
                    Sn = table.Column<string>(maxLength: 40, nullable: false),
                    ClasssId = table.Column<int>(nullable: false),
                    Father = table.Column<string>(nullable: true),
                    FathertTel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Student_Area_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Teacher",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Username = table.Column<string>(maxLength: 20, nullable: false),
                    Pswd = table.Column<string>(maxLength: 40, nullable: false),
                    Realname = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: false),
                    Tel = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false),
                    QQ = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    Nation = table.Column<string>(nullable: true),
                    IDCard = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    AreaId = table.Column<int>(nullable: true),
                    Available_balance = table.Column<double>(nullable: true),
                    Integral = table.Column<int>(nullable: false),
                    Gold = table.Column<int>(nullable: false),
                    Friend_num = table.Column<int>(nullable: false),
                    Attention_num = table.Column<int>(nullable: false),
                    Attentioned_num = table.Column<int>(nullable: false),
                    Credit = table.Column<int>(nullable: false),
                    Last_login_date = table.Column<DateTime>(nullable: false),
                    Login_date = table.Column<DateTime>(nullable: false),
                    Last_login_ip = table.Column<string>(nullable: true),
                    Login_ip = table.Column<string>(nullable: true),
                    Login_count = table.Column<int>(nullable: false),
                    IMEI = table.Column<string>(nullable: true),
                    Sn = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teacher", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teacher_Area_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Username = table.Column<string>(maxLength: 20, nullable: false),
                    Pswd = table.Column<string>(maxLength: 40, nullable: false),
                    Realname = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: false),
                    Tel = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false),
                    QQ = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    Nation = table.Column<string>(nullable: true),
                    IDCard = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    AreaId = table.Column<int>(nullable: true),
                    Available_balance = table.Column<double>(nullable: true),
                    Integral = table.Column<int>(nullable: false),
                    Gold = table.Column<int>(nullable: false),
                    Friend_num = table.Column<int>(nullable: false),
                    Attention_num = table.Column<int>(nullable: false),
                    Attentioned_num = table.Column<int>(nullable: false),
                    Credit = table.Column<int>(nullable: false),
                    Last_login_date = table.Column<DateTime>(nullable: false),
                    Login_date = table.Column<DateTime>(nullable: false),
                    Last_login_ip = table.Column<string>(nullable: true),
                    Login_ip = table.Column<string>(nullable: true),
                    Login_count = table.Column<int>(nullable: false),
                    IMEI = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Area_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderInfo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    ImageName = table.Column<string>(nullable: true),
                    Add_Time = table.Column<DateTime>(nullable: false),
                    Receipt_Time = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<string>(nullable: true),
                    Receive_Time = table.Column<DateTime>(nullable: false),
                    OrderDesc = table.Column<string>(nullable: true),
                    OrderTypeId = table.Column<int>(nullable: true),
                    OrderState = table.Column<string>(nullable: true),
                    AccountInfoId = table.Column<int>(nullable: true),
                    OrderArea = table.Column<string>(nullable: true),
                    OrderLevel = table.Column<int>(nullable: false),
                    OrderMoney = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderInfo_AccountInfo_AccountInfoId",
                        column: x => x.AccountInfoId,
                        principalTable: "AccountInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderInfo_OrderType_OrderTypeId",
                        column: x => x.OrderTypeId,
                        principalTable: "OrderType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Commodity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    ComName = table.Column<string>(nullable: true),
                    ComImage = table.Column<string>(nullable: true),
                    ComType = table.Column<string>(nullable: true),
                    BusinessId = table.Column<int>(nullable: true),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commodity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Commodity_Business_BusinessId",
                        column: x => x.BusinessId,
                        principalTable: "Business",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Area_ParentId",
                table: "Area",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Business_AccountInfoId",
                table: "Business",
                column: "AccountInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Commodity_BusinessId",
                table: "Commodity",
                column: "BusinessId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderInfo_AccountInfoId",
                table: "OrderInfo",
                column: "AccountInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderInfo_OrderTypeId",
                table: "OrderInfo",
                column: "OrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_AreaId",
                table: "Student",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_AreaId",
                table: "Teacher",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_User_AreaId",
                table: "User",
                column: "AreaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Commodity");

            migrationBuilder.DropTable(
                name: "Images");

            migrationBuilder.DropTable(
                name: "OrderInfo");

            migrationBuilder.DropTable(
                name: "SchoolTable");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "Teacher");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Business");

            migrationBuilder.DropTable(
                name: "OrderType");

            migrationBuilder.DropTable(
                name: "Area");

            migrationBuilder.DropTable(
                name: "AccountInfo");
        }
    }
}
