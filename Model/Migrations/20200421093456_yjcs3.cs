﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class yjcs3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ComImage",
                table: "Commodity");

            migrationBuilder.DropColumn(
                name: "ComName",
                table: "Commodity");

            migrationBuilder.DropColumn(
                name: "BuImage",
                table: "Business");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Commodity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pic",
                table: "Commodity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pic",
                table: "Business",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Commodity");

            migrationBuilder.DropColumn(
                name: "pic",
                table: "Commodity");

            migrationBuilder.DropColumn(
                name: "pic",
                table: "Business");

            migrationBuilder.AddColumn<string>(
                name: "ComImage",
                table: "Commodity",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ComName",
                table: "Commodity",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BuImage",
                table: "Business",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
