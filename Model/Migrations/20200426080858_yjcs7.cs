﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class yjcs7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ComType",
                table: "GoodsInfo");

            migrationBuilder.AddColumn<int>(
                name: "GoodsTypeId",
                table: "GoodsInfo",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GoodsType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Desc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GoodsInfo_GoodsTypeId",
                table: "GoodsInfo",
                column: "GoodsTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_GoodsInfo_GoodsType_GoodsTypeId",
                table: "GoodsInfo",
                column: "GoodsTypeId",
                principalTable: "GoodsType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GoodsInfo_GoodsType_GoodsTypeId",
                table: "GoodsInfo");

            migrationBuilder.DropTable(
                name: "GoodsType");

            migrationBuilder.DropIndex(
                name: "IX_GoodsInfo_GoodsTypeId",
                table: "GoodsInfo");

            migrationBuilder.DropColumn(
                name: "GoodsTypeId",
                table: "GoodsInfo");

            migrationBuilder.AddColumn<string>(
                name: "ComType",
                table: "GoodsInfo",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
