﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /// <summary>
    /// 账户信息实体类
    /// </summary>
    [Serializable]
    [Table("AccountInfo")]
    
    public class AccountInfo : ID
    {
        public AccountInfo() {
        }
        //微信ID WxID
        public string WxID { get; set; }
        //名称 Name
        public string AccountName { get; set; }
        //电话 Tel
        public string AccountTel { get; set; }
        //地址 Address
        public string AccountAddress { get; set; }
        //订单ID OrderID
        public string AccountPswd { get; set; }
    }
}
