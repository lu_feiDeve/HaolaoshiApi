﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /// <summary>
    /// 商家实体类
    /// </summary>
    [Serializable]
    [Table("OrderType")]
    
    public class OrderType : ID
    { 
        //订单类型名称
        public string TypeName { get; set; }
        //订单类型简介
        public string OrderTypeDesc { get; set; }
    }
}
