﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //学生表
    [Serializable]
    [Table("CommodityInfo")]
    public class CommodityInfo : ID
    {
        public string Name { get; set; }
        public string pic { get; set; }
        public string ComType { get; set; }

        public int? BusinessId { get; set; }
        [ForeignKey("BusinessId")]
        public virtual Business Business { get; set; }
        public int Price { get; set; }
    }
}