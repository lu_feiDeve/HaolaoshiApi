﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model
{
    /// <summary>
    /// 区域地址
    /// </summary>
    [Serializable]
    [Table("OrderInfo")]
    public class OrderInfo : ID
    {
        /// <summary>
        /// 订单信息表
        /// </summary>
        public string ImageName { get; set; }//图片地址
        public DateTime Add_Time { get; set; }//添加时间
        public DateTime Receipt_Time { get; set; }//接单时间
        public string Duration { get; set; }//等待时间
        public DateTime Receive_Time { get; set; }//确认时间
        public string OrderDesc { get; set; }//订单描述
        public int? OrderTypeId { get; set; }

        [ForeignKey("OrderTypeId")]//指定外键是这个字段

        public virtual OrderType OrderType { get; set; }//订单类型ID
        public string OrderState { get; set; }//订单状态
        public int? AccountInfoId { get; set; }

        [ForeignKey("AccountInfoId")]

        public virtual AccountInfo AccountInfo { get; set; }//用户信息
        public string OrderArea { get; set; }//委托地址
        public int OrderLevel { get; set; }//委托级别
        public int OrderMoney { get; set; }//订单金额
       
    }
}
