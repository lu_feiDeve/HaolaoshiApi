﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /// <summary>
    /// 账户信息实体类
    /// </summary>
    [Serializable]
    [Table("SchoolTable")]
    
    public class SchoolTable : ID
    { 
        //学校名称
        public string SchoolName { get; set; }
        //学校编号
        public int SchoolNumber { get; set; }
        //注册时间
        public string Register_Time { get; set; }
        //校区
        public string Partition { get; set; }
    }
}
