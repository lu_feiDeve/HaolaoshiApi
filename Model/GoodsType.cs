﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model
{
    /// <summary>
    /// 区域地址
    /// </summary>
    [Serializable]
    [Table("GoodsType")]
    public class GoodsType : ID
    {
        
        /// <summary>
        /// 商品类型表
        /// </summary>
        public string Name { get; set; }//类型名称
        public string Desc { get; set; }//类型简述
       
    }
}
