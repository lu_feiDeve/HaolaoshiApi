﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /// <summary>
    /// 商家实体类
    /// </summary>
    [Serializable]
    [Table("Business")]
    
    public class Business : ID
    { 
      
        //商家名称
        public string BuName { get; set; }
        //商家图片
        public string pic { get; set; }
        public int? AccountInfoId { get; set; }

        [ForeignKey("AccountInfoId")]
        //用户ID
        public virtual AccountInfo AccountInfo { get; set; }
      
        //商家地址
        public string BuAddress { get; set; }
        //许可证图片
        public string PermitImage { get; set; }
    }
}
