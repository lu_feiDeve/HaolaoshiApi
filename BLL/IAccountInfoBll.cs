﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace Bll
{
   
    public interface IAccountInfoBll : IBaseBll<AccountInfo>
    {
       public  AccountInfo Login(string nuserame, string pswd);
    }
}
