﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class BusinessBll : BaseBll<Business>, IBusinessBll
    {
        public BusinessBll(IBusinessDAL dal):base(dal)
        {
        }
    }
}
