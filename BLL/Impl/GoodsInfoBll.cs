﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class GoodsInfoBll : BaseBll<GoodsInfo>, IGoodsInfoBll
    {
        public GoodsInfoBll(IGoodsInfoDAL dal):base(dal)
        {
        }
    }
}
