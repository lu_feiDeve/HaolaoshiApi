﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class GoodsTypeBll : BaseBll<GoodsType>, IGoodsTypeBll
    {
        public GoodsTypeBll(IGoodsTypeDAL dal):base(dal)
        {
        }
    }
}
