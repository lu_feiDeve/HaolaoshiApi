﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
using Common.Util;
namespace Bll.Impl
{
    public class AccountInfoBll : BaseBll<AccountInfo>, IAccountInfoBll
    {
        public AccountInfoBll(IAccountInfoDAL dal) : base(dal)
        {
        }
        public new bool Add(AccountInfo o)
        {
            o.AccountPswd = Security.Md5(o.AccountPswd);
            return dal.Add(o);
        }
        public AccountInfo Login(string userame, string pswd)
        {
            pswd = Security.Md5(pswd);
            return dal.SelectOne(o => o.AccountName == userame && o.AccountPswd == pswd);
        }

    }
}
