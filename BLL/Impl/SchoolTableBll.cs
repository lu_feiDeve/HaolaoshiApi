﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class SchoolTableBll : BaseBll<SchoolTable>, ISchoolTableBll
    {
        public SchoolTableBll(ISchoolTableDAL dal):base(dal)
        {
        }
    }
}
