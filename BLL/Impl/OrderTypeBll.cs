﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class OrderTypeBll : BaseBll<OrderType>, IOrderTypeBll
    {
        public OrderTypeBll(IOrderTypeDAL dal):base(dal)
        {
        }
    }
}
