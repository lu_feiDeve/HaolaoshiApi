﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class OrderInfoBll : BaseBll<OrderInfo>, IOrderInfoBll
    {
        public OrderInfoBll(IOrderInfoDAL dal):base(dal)
        {
        }
    }
}
